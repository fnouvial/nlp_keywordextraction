# Text Summarization by Keywords/Keyphrases Extraction 

This code will help you to extract keywords or keyphrases from a text. In particular, it can be useful to summarize open-ended questions.

## Getting started

If you use the same way of storing data as I do (python dictionnary), you will probably only have to change some code in part 1.
/!\ Beware of typos/spelling mistakes in the answers (they should be corrected before the first step if you don't want words like 'pluseiurs' to come out as keyword)
To let you an example of the functioning of each function, I wrote few fake answers to a questionnaire.

pre-requisite: install jupyter notebook

Here are the steps of my method : 

## 1. Data Pre-processing
First, you need to upload your data. If your files are json, I gave you some help. 
To facilitate the following, you should use python's dictionaries to do like me.
The keys are the question's number ('questionb7' for example) and the value is the combination of all the participant's answers bonded end to end and separated by dots. Then, we only keep the open-ended questions that we want to summarize, to do this we use regular expressions. 
We also use another python's dictionary to store the correspondence between the question's number and the question's title.
Then, I calculated the length of the answers to the questions and I have defined a reduction rate which allows to know how many keywords to extract to reduce the size of the text. So I chose that the number of extracted keywords changes according to each question.

## 2. Use of different keywords extraction algorithm
I used two algorithms : 
-Rake : a statistical method which extract keywords (it could also extract longer ngrams if you change the range)
-Bert : a language model based on transformers, it extract n-grams between 1 and 4 words (you can change this parameter). These ngrams are extracted after that the stopwords have been removed from the text. The stopwords are the meaningless words like pronoms, prepositions... They are listed in 'stopwords_fr.txt'.

## 3. Get the context of a word
Sometimes, there are some keywords that are extracted that you can't understand without their context, so I wrote a function that allows you to write down the word and the question number so that you can read the sentence(s) from which that strange word comes.

## 4. Interpretation
To end, I kept the keywords and keyphrases extracted for each question and I added few sentences to summarize the text using extracted keywords.

Attached files : 
- stopwords_fr.txt : is the list of meaningless words (determiners, pronouns) that we delete from our text before keywords extraction

If you have some questions, feel free to contact me : sarah.oury@insa-rennes.fr
